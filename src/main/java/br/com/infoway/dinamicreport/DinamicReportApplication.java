package br.com.infoway.dinamicreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DinamicReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(DinamicReportApplication.class, args);
	}

}
